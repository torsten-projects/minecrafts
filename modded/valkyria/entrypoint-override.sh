#!/bin/bash

declare -x WORLD_TYPE;
[[ -z "${WORLD_TYPE}" ]] && WORLD_TYPE="DEFAULT";

declare -x WORLD_DIFFICULTY;
[[ -z "${WORLD_DIFFICULTY}" ]] && WORLD_DIFFICULTY="2";

declare -x MOTD;
[[ -z "${MOTD}" ]] && MOTD="Valkyria on Docker, by Freem~";

declare -x RAM;
[[ -z "${RAM}" ]] && RAM="16G";

declare -x RAMDISK;
[[ -z "${RAMDISK}" ]] && RAMDISK=false;

declare -x ONLINE;
[[ -z "${ONLINE}" ]] && ONLINE=true;

echo "World type: $WORLD_TYPE";
echo "World difficulty: $WORLD_DIFFICULTY";
echo "MOTD: $MOTD";
echo "RAM: $RAM";
echo "Use ramdisk: $RAMDISK";

sed -i s/LEVEL_TYPE/$WORLD_TYPE/g /data/server.properties;
sed -i s/WORLD_DIFFICULTY/$WORLD_DIFFICULTY/g /data/server.properties;
sed -i s/ONLINE_MODE/$ONLINE/g /data/server.properties;
sed -i "s/MESSAGE_OF_THE_DAY/$MOTD/g" /data/server.properties;

echo "Starting the server";

cd /data && java -Xms$RAM -Xmx$RAM -Dfml.queryResult=confirm @libraries/net/minecraftforge/forge/1.18.1-39.0.46/unix_args.txt;

echo "Stopped";
